#include <iostream>
#include <initializer_list>
#include <vector>
#include <set>
#include <iterator>
#include <stdexcept>

using namespace std;

class Data
{
    vector<int> data_;
public:
    Data(std::initializer_list<int> il) : data_ {il}
    {
        cout << "Data(il)" << endl;
    }

    Data() = delete;

    ~Data() = default;

    const vector<int>& data() const
    {
        return data_;
    }
};

class Base
{
public:
    virtual void do_sth() { cout << "Base::do_sth()" << endl; }
    virtual ~Base() = default;
};

class Derived : public Base
{
public:
    //void do_sth() = delete;
};

struct X
{
    int a;
    double b;
};

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : cont)
        cout << item << " ";
    cout << "]" << endl;
}

template <typename T, typename Cmp = std::less<T>>
struct IndexableSet : std::set<T, Cmp>
{
    using set<T, Cmp>::set; // dziedziczenie konstruktorów

    template <typename InIt>
    IndexableSet(InIt start, InIt end)
    {
        cout << "Overloaded ctor" << endl;

        for(auto it = start; it != end; ++it)
            this->insert(*it);
    }

    const T& operator[](size_t index) const
    {
        return this->at(index);
    }

    const T& at(size_t index) const
    {
        if (index >= this->size())
            throw out_of_range("error: index out of range");

        auto iter = this->begin();
        advance(iter, index);

        return *iter;
    }
};

int main()
{
    X aggr = { 2, 4.0 };

    auto geek = { [](){} }; // C++11 :)

    int x {};

    Data data = { 2, 3, 4, 5, 6 };

    print(data.data(), "data");

    Data other_data = move(data); // kopiowanie data

    IndexableSet<int> iset = { 1, 2, 4, 5, 6, 3, -2, 3 };

    cout << "iset[2]: " << iset[2] << endl;

    IndexableSet<int> other_set { iset.rbegin(), iset.rend() };

    cout << "other_set[2]: " << other_set[2] << endl;

}
