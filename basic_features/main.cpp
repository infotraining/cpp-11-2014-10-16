#include <iostream>
#include <typeinfo>
#include <vector>
#include <set>
#include <array>
#include <iterator>
#include <thread>
#include <chrono>

using namespace std;

void fun1(int* ptr)
{
    cout << "fun_with_ptr(int*: ";

    if (ptr)
       cout << *ptr;
    else
       cout << ptr;

    cout << ")" << endl;
}

void fun1(long value)
{
    cout << "fun1(int: " << value << ")" << endl;
}

void fun1(nullptr_t ptr)
{
    cout << "fun1(nullptr)" << endl;
}

template <typename T>
void foo(T item1, T item2)
{
    cout << "foo(" <<  typeid(item1).name()
         << ", " <<  typeid(item2).name() << ")" << endl;
}

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : cont)
        cout << item << " ";
    cout << "]" << endl;
}

auto i = 0L;
auto x = 0;

auto foobar(int arg1, int arg2) -> decltype(arg1 + arg2)
{
    return arg1 + arg2;
}

int get_value()
{
    return 10;
}

int main()
{
    this_thread::sleep_for(chrono::milliseconds(100));
    this_thread::sleep_for(100ms);

    cout << "foobar(3): " << foobar(3, 7) << endl;
    cout << "foobar(0): " << foobar(0, 5) << endl;

    int x = 10;

    fun1(&x);
    fun1(nullptr);
    fun1(10);

    int* ptr {};

    auto il = { 1, 2, 3, 4, 5 }; // il: std::initializer_list<int>

    foo(il, il);

    //foo({1, 2, 3, 4, 5}); // error: in this context {1, 2, 3, 4, 5} is not a type

    foo<int>(1, 1.0);

    vector<string> vec = { "one", "two", "three" };

    for(const auto& word : vec)
    {
        cout << word << " ";
    }

    int tab[] = { 1, 2, 3, 4, 5 };

    for(auto& item : tab)
        item += 2;

    for(const auto& item : {"four", "five", "six"})
        vec.push_back(item);

    for(const auto& word : vec)
    {
        cout << word << " ";
    }

    const vector<int> numbers = {1, 2, 3, 4 };

    for(const auto& item : numbers)
        cout << item;


    vector<int> v1 {};
    print(v1, "v1");

    vector<int> v2(10, -1);  // vector<int>(size_t)
    print(v2, "v2");

    vector<int> v3 {10, -1}; // vector<int>(initializer_list)
    print(v3, "v3");

    vector<string> words {10, "hello"};
    print(words, "words");

    int arr98[10] = { 1, 2, 3, 4 };

    for(auto it = begin(arr98); it != end(arr98); ++it)
        cout << *it << " ";
    cout << endl;
}
