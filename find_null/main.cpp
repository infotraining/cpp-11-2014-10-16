#include <iostream>
#include <vector>
#include <iterator>
#include <memory>

using namespace std;

template <typename Container>
auto find_null(const Container& c) -> decltype(begin(c))
{
    auto it = begin(c);

    for(; it != end(c); ++it)
        if (*it == nullptr)
            break;
    return it;
}


int main()
{

    // find_null: Given a container of pointers, return an
    // iterator to the first null pointer (or the end
    // iterator if none is found)

    vector<int*> ptrs = { new int {}, new int {10}, nullptr, new int {45}, new int {20} };

    auto where_null1 = find_null(ptrs);

    cout << "Index: " << distance(ptrs.cbegin(), where_null1) << endl;

    for(auto& ptr : ptrs)
        delete ptr;


    auto shared_ptrs = { make_shared<int>(10), nullptr, shared_ptr<int>{}, make_shared<int>(20) };

    auto where_null2 = find_null(shared_ptrs);

    cout << "Index of null: " << distance(shared_ptrs.begin(), where_null2) << endl;
}
